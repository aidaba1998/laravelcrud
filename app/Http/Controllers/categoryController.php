<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class categoryController extends Controller
{
    public function index(){
        $categories=Category::latest()->paginate(3);
        return view ('categories.liste',['categories'=>$categories]);

    }
    public function create(){
        return view ('categories.new');
    }
    public function  store(Request $request){
        //validation data
        $request->validate([
         'title'=>'required|unique:categories|max:200'
        ]);
     $data=new Category; 
     $data->title=$request->title;
     $data->save();
     return redirect('/')->withSuccess('New Category Created');
    }

    public function  edit($id){
     $category=Category::where('id',$id)->first();
     return view('categories.edit',['category'=>$category]);
    }
    public function update(Request $request,$id){
        $category=Category::where('id',$id)->first();
        $category->title=$request->title;
        $category->save();
        return \redirect('/');


    }
    public function destroy($id){
        $category=Category::whereId($id)->first();
        $category->delete();
        return redirect('/');
    }
    

     
}