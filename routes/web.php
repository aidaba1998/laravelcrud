<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\categoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[categoryController::class,'index']);
Route::get('/category-create',[categoryController::class,'create']);
Route::post('/category-store',[categoryController::class,'store']);
Route::get('/category-edit/{id}',[categoryController::class,'edit']);
Route::put('/category-update/{id}',[categoryController::class,'update']);
Route::delete('/category-delete/{id}',[categoryController::class,'destroy']);